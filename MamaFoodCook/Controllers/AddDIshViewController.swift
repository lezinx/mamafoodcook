//
//  AddDIshViewController.swift
//  MamaFoodCook
//
//  Created by Ziong on 2/24/19.
//  Copyright © 2019 Ziong. All rights reserved.
//

import UIKit
import Firebase

protocol AddDishViewControllerDelegate {
    func addDish(dish: Dish)
}

class AddDishViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var delegate: AddDishViewControllerDelegate!
    let ref = Database.database().reference(withPath: "users")
    let imagePicker = UIImagePickerController()
    @IBOutlet weak var titleDish: UITextField!
    @IBOutlet weak var descriptionDish: UITextView! {
        didSet {
            descriptionDish.delegate = self
        }
    }
    @IBOutlet weak var priceDish: UITextField!
    @IBOutlet weak var imageDish: UIImageView! {
        didSet {
            imageDish.isUserInteractionEnabled = true
            imageDish.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(imageViewPressed)))
        }
    }
    
    @objc func imageViewPressed() {
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        
        present(imagePicker, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self
        descriptionDish.text = "Placeholder"
        descriptionDish.textColor = UIColor.lightGray
        // Do any additional setup after loading the view.
    }
    
    @IBAction func saveButtonPressed(_ sender: Any) {
        guard let title = titleDish.text, let price = priceDish.text, let description = descriptionDish.text, let image = imageDish.image else {
            return
        }
        let dish = Dish(name: title, imgName: image, price: price, description: description)
        
        guard let user = Auth.auth().currentUser else {
            return
        }
        let dishRef = self.ref.child(user.uid)
        dishRef.setValue(dish.toAnyObject())
        
        

        delegate?.addDish(dish: dish)
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        imageDish.image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    

}

extension AddDishViewController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = ""
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Placeholder"
            textView.textColor = UIColor.lightGray
        }
    }
    
}
