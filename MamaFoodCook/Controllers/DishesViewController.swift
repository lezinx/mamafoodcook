//
//  DishesViewController.swift
//  MamaFoodCook
//
//  Created by Ziong on 2/23/19.
//  Copyright © 2019 Ziong. All rights reserved.
//

import Foundation
import UIKit

class DishesViewController: UIViewController {
    
    var dishes = [Dish]()
    
    @IBOutlet weak var collectionView: UICollectionView! {
        didSet {
            collectionView.dataSource = self
            collectionView.delegate = self
        }
    }
    
    @IBAction func addDishBarButtonPresed(_ sender: Any) {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        if let addDishViewController = mainStoryboard.instantiateViewController(withIdentifier: "AddDishViewController") as? AddDishViewController {
            addDishViewController.delegate = self
            self.navigationController?.present(addDishViewController, animated: true, completion: nil)
        }
    }
    
    @IBAction func editButtonPressed(_ sender: Any) {
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
}

extension DishesViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dishes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let dishCell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? DishCollectionViewCell {
            dishCell.dish = dishes[indexPath.item]
            return dishCell
        }
        return UICollectionViewCell()
    }
}

extension DishesViewController: AddDishViewControllerDelegate {
    
    func addDish(dish: Dish) {
        self.dishes.append(dish)
        self.collectionView.reloadData()
    }
    
}
