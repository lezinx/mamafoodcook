import UIKit

class DishCollectionViewCell: UICollectionViewCell {
    
    
    var dish: Dish? {
        didSet {
            titleTextField.text = dish?.name
            descriptionTextView.text = dish?.description
            imageView.image = dish?.imgName
            priceTextField.text = dish?.price
            
            titleTextField.isUserInteractionEnabled = false
            descriptionTextView.isUserInteractionEnabled = false
            priceTextField.isUserInteractionEnabled = false
            saveButton.isEnabled = false
            saveButton.alpha = 0.3
        }
    }
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var priceTextField: UITextField!
    @IBOutlet weak var saveButton: UIButton! {
        didSet {
            saveButton.layer.borderColor = UIColor.black.cgColor
            saveButton.layer.borderWidth = 1.0
            saveButton.layer.masksToBounds = true
            saveButton.layer.cornerRadius = 10
        }
    }
    
    @IBAction func saveButtonPressed(_ sender: Any) {
        setCellIsEditable(isEditable: false)
    }
    
    func setCellIsEditable(isEditable: Bool) {
        if isEditable == false {
            titleTextField.isUserInteractionEnabled = false
            descriptionTextView.isUserInteractionEnabled = false
            priceTextField.isUserInteractionEnabled = false
            saveButton.isEnabled = false
            saveButton.alpha = 0.3
        } else {
            titleTextField.isUserInteractionEnabled = true
            descriptionTextView.isUserInteractionEnabled = true
            priceTextField.isUserInteractionEnabled = true
            saveButton.isEnabled = true
            saveButton.alpha = 1.0
        }
    }
    
}

extension DishCollectionViewCell: UIImagePickerControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let tempImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            self.imageView.image = tempImage
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
}
