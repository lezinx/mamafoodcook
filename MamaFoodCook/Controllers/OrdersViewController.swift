import Foundation
import UIKit
import Firebase


class OrdersViewController: UIViewController {
    
    let ref = Database.database().reference(withPath: "1")
    
    var orders = [Order(client: "Иван", dishes: ["Рататуй","Индейка с яблоками и луком"], totalPrice: "100"),Order(client: "Петя", dishes: ["Картофель «Айдахо"], totalPrice: "40")]

    @IBOutlet weak var collectionView: UICollectionView! {
        didSet {
            collectionView.dataSource = self
            collectionView.delegate = self
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetch()
    }
    
    private func fetch() {
        ref.observe(.value) { snapshot in
            var newDishes: [DishAPIModel] = []
            
            for child in snapshot.children {
                if let snapshot = child as? DataSnapshot {
                    let dish = DishAPIModel(id: snapshot.value(forKey: "id") as! Int,
                                            title: snapshot.value(forKey: "title") as! String,
                                            imageLinks: [snapshot.value(forKey: "imageLinks") as! String],
                                            type: "FOOD",
                                            price: 45.0,
                                            description: snapshot.value(forKey: "id") as! String,
                                            available: (snapshot.value(forKey: "available") != nil),
                                            deleted: (snapshot.value(forKey: "deleted") != nil))
                    print(dish)
                }
            }
        }
        
    }
    
}

extension OrdersViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return orders.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let orderCell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellid", for: indexPath) as? OrderCollectionViewCell {
//            orderCell.order = orders[indexPath.item]
            return orderCell
        }
        return UICollectionViewCell()
    }
}
