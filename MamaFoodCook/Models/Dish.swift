//
//  Dish.swift
//  MamaFoodCook
//
//  Created by Ziong on 2/24/19.
//  Copyright © 2019 Ziong. All rights reserved.
//

import UIKit

struct Dish {
    var name: String?
    var imgName: UIImage?
    var price: String?
    var description: String?
    
    init(name: String, imgName: UIImage, price: String, description: String) {
        self.name = name
        self.imgName = imgName
        self.price = price
        self.description = description
    }
    
    
    func toAnyObject() -> Any {
        let imageData:Data = imgName!.pngData()!
        let strBase64 = imageData.base64EncodedString(options: .lineLength64Characters)
        
        
        return [
            "name": name,
            "img": strBase64,
            "description": description,
            "price": price
        ]
        
    }
    
    
  
}
