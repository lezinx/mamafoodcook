//
//  User.swift
//  MamaFoodCook
//
//  Created by Ziong on 2/24/19.
//  Copyright © 2019 Ziong. All rights reserved.
//

import Foundation
import UIKit
struct User {
    var fullName: String?
    var phoneNumber: String?
    var email: String?
    var image: UIImage?
    
    init(fullName: String, phoneNumber: String, email: String, image: UIImage) {
        self.fullName = fullName
        self.phoneNumber = phoneNumber
        self.email = email
        self.image = image
    }
    
    func toAnyObject() -> Any {
        let imageData:Data = image!.pngData()!
        let strBase64 = imageData.base64EncodedString(options: .lineLength64Characters)
        
        return [
            "fullName": fullName,
            "phoneNumber": phoneNumber,
            "email": email,
            "image": strBase64
        ]
        
    }
}
