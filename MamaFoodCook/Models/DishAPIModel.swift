import Firebase

struct DishAPIModel: Decodable {
    let ref: DatabaseReference?
    let key: String
    var id: Int
    var title: String!
    var imageLinks: [String]!
    var type: String!
    var price: Double!
    var description: String!
    var available: Bool
    var deleted: Bool
    
    init(id: Int, title: String, imageLinks: [String], type: String, price: Double, description: String, available: Bool, deleted: Bool,key: String = "") {
        self.id = id
        self.title = title
        self.imageLinks = imageLinks
        self.type = type
        self.price = price
        self.description = description
        self.available = available
        self.deleted = deleted
        self.ref = nil
        self.key = key
        
    }
    
    init?(snapshot: DataSnapshot) {
        guard
            let value = snapshot.value as? [String: AnyObject],
            let name = value["name"] as? String,
            let addedByUser = value["addedByUser"] as? String,
            let completed = value["completed"] as? Bool else {
                return nil
        }
        
        self.ref = snapshot.ref
        self.key = snapshot.key
        self.id = id
        self.title = title
        self.imageLinks = imageLinks
        self.type = type
        self.price = price
        self.description = description
        self.available = available
        self.deleted = deleted
    }
}
