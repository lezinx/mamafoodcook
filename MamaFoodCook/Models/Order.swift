//
//  Order.swift
//  MamaFoodCook
//
//  Created by Ziong on 2/24/19.
//  Copyright © 2019 Ziong. All rights reserved.
//

import UIKit

struct Order {
    var client: String?
    var dishes: [String]?
    var totalPrice: String?
    
    init(client: String, dishes: [String], totalPrice: String) {
        self.client = client
        self.dishes = dishes
        self.totalPrice = totalPrice
    }
}

